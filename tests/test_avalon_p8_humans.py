"""
    Test the game itself.
"""
from app import constants
from . import PlayGameManager


def test_start_vanilla_game():
    """ Test to see if we can start a normal game with 8 people """
    game = PlayGameManager(8)

    game.disconnect()
